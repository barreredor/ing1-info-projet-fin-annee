package exceptions;

/**
 * Cette exception est lev�e lorsqu'aucune image n'est affich�e.
 */
public final class AucuneImageAfficheeException extends RuntimeException {
    private static final long serialVersionUID = -8580122030045308789L;

    /**
     * Constructeur prenant en param�tre un message d'erreur.
     * @param msg Le message d'erreur associ� � cette exception.
     */
    public AucuneImageAfficheeException(String msg) {
        super(msg);
    }
}