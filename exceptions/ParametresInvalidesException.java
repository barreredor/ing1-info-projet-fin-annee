package exceptions;

/**
 * Cette exception est lev�e lorsqu'un ou plusieurs param�tres sont invalides.
 */
public final class ParametresInvalidesException extends RuntimeException {
    private static final long serialVersionUID = -8832538949978334866L;

    /**
     * Constructeur prenant en param�tre un message d'erreur.
     * @param msg Le message d'erreur associ� � cette exception.
     */
    public ParametresInvalidesException(String msg) {
        super(msg);
    }
}