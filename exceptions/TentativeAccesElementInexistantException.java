package exceptions;

/**
 * Cette exception est lev�e lorsqu'une tentative d'acc�s � un �l�ment inexistant est d�tect�e.
 */
public final class TentativeAccesElementInexistantException extends RuntimeException {
    private static final long serialVersionUID = -4920563053558154091L;

    /**
     * Constructeur prenant en param�tre un message d'erreur.
     * @param msg Le message d'erreur associ� � cette exception.
     */
    public TentativeAccesElementInexistantException(String msg) {
        super(msg);
    }
}