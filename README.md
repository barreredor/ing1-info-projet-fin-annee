# ING1 Info Projet Fin Année : Le Morphing

Projet de fin d'année en informatique effectué dans le cadre de l'année 2023/2024 en ING1, mis à jour le 06 mai 2024.

## Participants (Groupe 4)

- Lilian DELETOILE
- Camille SOLACROUP
- Guillaume GADZINA
- Florent CRAHAY-BOUDOU
- Dorian BARRERE

## Contacts

- Dorian BARRERE <barreredorian332@gmail.com>
- Lilian Deletoile <deletoilel@cy-tech.fr>
- Camille Solacroup <solacroupc@cy-tech.fr>
- Guillaume Gadzina <gadzinagui@cy-tech.fr>
- Florent Crahay-Boudou <crahayboud@cy-tech.fr>

## Description du Dossier de Rendu

Le dossier de rendu (`ing1-gi-groupe-04`) contient les éléments suivants :

- `jreJavaFX_Linux` : Contient l'environnement d'exécution JavaFX pour les systèmes Linux. Utilisé pour lancer l'application sur un système Linux.
- `jreJavaFX_Windows` : Contient l'environnement d'exécution JavaFX pour les systèmes Windows. Utilisé pour lancer l'application sur un système Windows.
- `lanceur_Linux.sh` : Le fichier script permettant de lancer l'application sur les systèmes Linux.
- `lanceur_Windows.bat` : Le fichier script permettant de lancer l'application sur les systèmes Windows.
- `executable.jar` : Le fichier JAR exécutable de l'application.
- `sources.jar` : Contient les sources lisibles de l'application.
- `rapport.*` : Le rapport du projet.

Ces dossiers permettent une indépendance totale de l'application vis-à-vis du système d'exploitation, tant qu'il est de type Linux ou Windows.

## Instructions pour Lancer l'Application

### Sur Windows

Pour lancer l'application sur Windows, doubler-cliquer sur le fichier .bat fourni (`lanceur_Windows.bat`).

### Sur Linux

Pour lancer l'application sur Linux, 2 méthodes sont possibles :

- Clic droit sur le fichier .sh fourni (`lanceur_Linux.sh`), puis `Run as a program`.
- Depuis un terminal, taper la commande `./lanceur_Linu`.

## Environnement de Développement Utilisé

- Version du Java SDK utilisé pour compiler : `Java JDK 20`
- Version du JavaFX SDK : `JavaFX SDK 22`