package application.presentation;

import application.controle.ControleBoutonGenererMorphing;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Cette classe repr�sente une interface sp�cifique pour le morphing avec des images.
 */
public final class InterfaceMorphingImages extends InterfaceMorphingAvecDonnneesMorphingAvecPointControleDoubles {
	
    @Override
    protected Label creerTitre() {
        // Cr�e un titre sp�cifique pour cette interface.
        return new Label("Mode Images");
    }
    
    @Override
    protected ControleBoutonGenererMorphing creerControleBoutonGenererMorphing(Button boutonGenererMorphing) {
        // Cr�e un contr�le pour le bouton de g�n�ration du morphing.
        return new ControleBoutonGenererMorphing(this, boutonGenererMorphing, ControleBoutonGenererMorphing.MORPHING_IMAGES);
    }
    
    @Override
    protected String genererAidePlacerPointsDeControle() {
        // G�n�re un texte d'aide pour placer les points de contr�le sp�cifique � cette interface.
        return
                  "- Pour placer un point de contr�le, cliquez avec le bouton gauche de la souris sur l'image.\n\n"
                + "- Pour d�placer un point de contr�le, maintenez le bouton gauche de la souris enfonc� et d�placez la souris.\n\n"
                + "- Pour d�finir une ligne de contr�le, un couple de 2 points de contr�le est n�cessaire.\n\n"
                + "- Placer en suivant les deux points de cont�le afin de cr�er une ligne de contr�le.";
    }
}