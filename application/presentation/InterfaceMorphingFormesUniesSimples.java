package application.presentation;

import application.controle.ControleBoutonGenererMorphing;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * Cette classe repr�sente une interface sp�cifique pour le morphing avec des formes unies simples.
 */
public final class InterfaceMorphingFormesUniesSimples extends InterfaceMorphingAvecDonnneesMorphingAvecPointControleSimples {

    @Override
    protected Label creerTitre() {
        // Cr�e un titre sp�cifique pour cette interface.
        return new Label("Mode Formes Unies Simples");
    }
    
    @Override
    protected ControleBoutonGenererMorphing creerControleBoutonGenererMorphing(Button boutonGenererMorphing) {
        // Cr�e un contr�le pour le bouton de g�n�ration du morphing.
        return new ControleBoutonGenererMorphing(this, boutonGenererMorphing, ControleBoutonGenererMorphing.MORPHING_FORMES_UNIES_SIMPLES);
    }
    
    @Override
    protected void mettreAJourAffichageLignesEntrePointsDeControle(Pane boiteImage) {
        // Met � jour l'affichage des lignes entre les points de contr�le.
        if (boiteImage.getChildren().size() > 1 + getDonneesAssociees().getNombrePointsControle()) {
            boiteImage.getChildren().remove(1, boiteImage.getChildren().size() - getDonneesAssociees().getNombrePointsControle());
        }
        
        for (int i = 2 ; i < boiteImage.getChildren().size() ; i += 2) {
            // Cr�e une ligne entre deux n�uds de contr�le et les ajoute � la bo�te d'image.
            boiteImage.getChildren().add(i/2, creerLigneEntreDeuxNoeuds(boiteImage.getChildren().get(i-1), boiteImage.getChildren().get(i)));
        }
    }

    @Override
    protected String genererAidePlacerPointsDeControle() {
        // G�n�re un texte d'aide pour placer les points de contr�le sp�cifique � cette interface.
        return
                  "- Pour placer un point de contr�le, cliquez avec le bouton gauche de la souris sur l'image.\n\n"
                + "- Pour d�placer un point de contr�le, maintenez le bouton gauche de la souris enfonc� et d�placez la souris.\n\n"
                + "- Pour g�n�rer le morphing, assurez-vous que le point d'arriv�e soit �quivalent au point de d�part pour toutes les images.";
    }
}