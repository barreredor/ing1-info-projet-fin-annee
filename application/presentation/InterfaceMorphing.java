package application.presentation;

import java.util.List;
import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.abstraction.PointControle;
import application.abstraction.PointControleSimple;
import application.controle.ControleListView;
import application.controle.ControleBoutonGenererMorphing;
import application.controle.ControleChangementImages;
import application.controle.ControleChangementTailleBoiteImage;
import application.controle.ControleMenuItemDecalageImageActuelle;
import application.controle.ControleMenuItemAjoutOuModificationImages;
import application.controle.ControleMenuItemSuppressionImages;
import application.controle.ControleMenuItemSuppressionPointsDeControle;
import application.controle.ControleMode;
import application.controle.ControleChangementPointsDeControle;
import exceptions.AucuneImageAfficheeException;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.control.ProgressIndicator;
import javafx.concurrent.Task;

/**
 * Classe abstraite repr�sentant l'interface utilisateur pour le morphing d'images.
 * @param <T> Le type de point de contr�le utilis� dans le morphing.
 */
public abstract class InterfaceMorphing<T extends PointControle> extends Observable {
    
    public static final Integer MISE_A_JOUR_AFFICHAGE_POINTS_CONTROLE = 1;

    protected DonneesMorphing<T> donneesAssociees;
    private BorderPane root;
    private VBox bandeauHaut;
    private GridPane partieCentrale;
    private HBox bandeauBas;
    private Scene scene;
    private double facteurDeZoomDesImagesAffichees;
    private boolean unNoeudPointDeControleVientDEtreDeplace;
    private ColorPicker colorPicker;
    private Color couleurPointDeControle;
    private TextField textFieldNombreImagesIntermediaires;
    private TextField textFieldDureeGIF;

    /**
     * Constructeur de la classe InterfaceMorphing initialisant les composants de l'interface.
     */
    public InterfaceMorphing() {
        donneesAssociees = creerDonneesAssociee();
        donneesAssociees.getListeObservateurs().add(new ControleChangementPointsDeControle(this));
        donneesAssociees.getListeObservateurs().add(new ControleChangementImages(this));

        root = new BorderPane();
        bandeauHaut = creerBandeauHaut();
        couleurPointDeControle = colorPicker.getValue();
        partieCentrale = creerPartieCentrale();
        bandeauBas = creerBandeauBas();
        scene = genererScene();
        facteurDeZoomDesImagesAffichees = 1;
        unNoeudPointDeControleVientDEtreDeplace = false;	
    }

    /**
     * Cr�e les donn�es associ�es pour le morphing.
     * @return Les donn�es associ�es.
     */
    protected abstract DonneesMorphing<T> creerDonneesAssociee();

    /**
     * Retourne les donn�es associ�es pour le morphing.
     * @return Les donn�es associ�es.
     */
    public DonneesMorphing<T> getDonneesAssociees() {
        return donneesAssociees;
    }

    /**
     * Retourne la partie centrale de l'interface.
     * @return La partie centrale de l'interface.
     */
    private GridPane getPartieCentrale() {
        return partieCentrale;
    }

    /**
     * Retourne la racine de la sc�ne.
     * @return La racine de la sc�ne.
     */
    public BorderPane getRoot() {
        return root;
    }

    /**
     * Retourne la sc�ne de l'interface.
     * @return La sc�ne de l'interface.
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * Retourne le conteneur de l'image centrale.
     * @return Le conteneur de l'image centrale.
     */
    public Pane getBoiteImageCentrale() {
        return (Pane) ((BorderPane) getPartieCentrale().getChildren().get(1)).getCenter();
    }

    /**
     * Retourne la translation en X de l'image dans son conteneur.
     * @param boiteImage Le conteneur de l'image.
     * @return La translation en X de l'image.
     */
    public static double getTranslationXImageDansBoiteImage(Pane boiteImage) {
        if (boiteImageContientUneImage(boiteImage)) {
            return (boiteImage.getWidth() - getImageView(boiteImage).getBoundsInParent().getWidth()) / 2;
        } else {
            return 0;
        }
    }

    /**
     * Retourne la translation en Y de l'image dans son conteneur.
     * @param boiteImage Le conteneur de l'image.
     * @return La translation en Y de l'image.
     */
    public static double getTranslationYImageDansBoiteImage(Pane boiteImage) {
        if (boiteImageContientUneImage(boiteImage)) {
            return (boiteImage.getHeight() - getImageView(boiteImage).getBoundsInParent().getHeight()) / 2;
        } else {
            return 0;
        }
    }

    /**
     * Retourne l'ImageView contenue dans le conteneur sp�cifi�.
     * @param boiteImage Le conteneur de l'image.
     * @return L'ImageView contenue dans le conteneur, ou null si aucune ImageView n'est pr�sente.
     */
    public static ImageView getImageView(Pane boiteImage) {
        if (boiteImage.getChildren().get(0) instanceof ImageView) {
            return (ImageView) boiteImage.getChildren().get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retourne le facteur de zoom des images affich�es.
     * @return Le facteur de zoom des images affich�es.
     */
    public double getFacteurDeZoomDesImagesAffichees() {
        return facteurDeZoomDesImagesAffichees;
    }

    /**
     * Retourne la couleur du point de contr�le.
     * @return La couleur du point de contr�le.
     */
    private Color getCouleurPointDeControle() {
        return couleurPointDeControle;
    }

    /**
     * Retourne le nombre d'images interm�diaires.
     * @return Le nombre d'images interm�diaires.
     */
    public int getNombreImagesIntermediaires() {
        return Integer.parseInt(textFieldNombreImagesIntermediaires.getText());
    }

    /**
     * Retourne la dur�e du GIF.
     * @return La dur�e du GIF.
     */
    public double getDureeGIF() {
        return Double.parseDouble(textFieldDureeGIF.getText());
    }

    /**
     * D�finit la couleur du point de contr�le.
     * @param nouvelleCouleurPointDeControle La nouvelle couleur du point de contr�le.
     */
    private void setCouleurPointDeControle(Color nouvelleCouleurPointDeControle) {
        couleurPointDeControle = nouvelleCouleurPointDeControle;
    }

    /**
     * Indique si un noeud point de contr�le vient d'�tre d�plac�.
     * @return Vrai si un noeud point de contr�le vient d'�tre d�plac�, sinon faux.
     */
    private boolean unNoeudPointControleVientDEtreDeplace() {
        return unNoeudPointDeControleVientDEtreDeplace;
    }

    /**
     * D�finit si un noeud point de contr�le vient d'�tre d�plac�.
     * @param unNoeudPointDeControleVientDEtreDeplace Vrai si un noeud point de contr�le vient d'�tre d�plac�, sinon faux.
     */
    private void unNoeudPointControleVientDEtreDeplace(Boolean unNoeudPointDeControleVientDEtreDeplace) {
        this.unNoeudPointDeControleVientDEtreDeplace = unNoeudPointDeControleVientDEtreDeplace;
    }

    /**
     * Met � jour le facteur de zoom de l'image affich�e.
     * @throws AucuneImageAfficheeException Si aucune image n'est affich�e.
     */
    public void setFacteurZoomDeImageAffichee() {
        if (uneImageEstAffichee()) {
            facteurDeZoomDesImagesAffichees = getImageView(getBoiteImageCentrale()).getBoundsInParent().getWidth() / getDonneesAssociees().getImageActuelle().getWidth();
        } else {
            throw new AucuneImageAfficheeException("Une image doit �tre affich�e afin de pouvoir calculer le facteur de zoom.");
        }
    }

    /**
     * V�rifie si le conteneur d'image contient une image.
     * @param boiteImage Le conteneur d'image.
     * @return Vrai si le conteneur contient une image, sinon faux.
     */
    public static Boolean boiteImageContientUneImage(Pane boiteImage) {
        return boiteImage != null && boiteImage.getChildren().size() > 0 && boiteImage.getChildren().get(0) instanceof ImageView && ((ImageView) boiteImage.getChildren().get(0)).getImage() != null;
    }

    /**
     * Cr�e le bandeau sup�rieur de l'interface.
     * @return Le bandeau sup�rieur de l'interface.
     */
    private VBox creerBandeauHaut() {
        Label titre = creerTitre();
        titre.getStyleClass().add("titre");
        titre.setPadding(new Insets(20, 20, 0, 20));

        VBox bandeauHaut = new VBox(creerMenu(), titre);
        bandeauHaut.setAlignment(javafx.geometry.Pos.CENTER);
        return bandeauHaut;
    }

    /**
     * Cr�e le titre pour le bandeau sup�rieur.
     * @return Le titre pour le bandeau sup�rieur.
     */
    protected abstract Label creerTitre();
    
    /**
     * Cr�e le menu de l'interface.
     * @return Le menu de l'interface.
     */
    private MenuBar creerMenu() {
    	// Cr�ation du menu "Application"
        Menu menuApplication = new Menu("Application");

        // Cr�ation du menu "Modes" avec ses �l�ments
        Menu menuModes = new Menu("Modes");
        MenuItem modeFormesUniesSimples = new MenuItem("Formes Unies Simples");
        modeFormesUniesSimples.setOnAction(e -> ControleMode.changerVersModeFormesUniesSimples(false));
        MenuItem modeFormesUniesArrondies = new MenuItem("Formes Unies Arrondies");
        modeFormesUniesArrondies.setOnAction(e -> ControleMode.changerVersModeFormesUniesArrondies(false));
        MenuItem modeImages = new MenuItem("Images");
        modeImages.setOnAction(e -> ControleMode.changerVersModeImages(false));
        menuModes.getItems().addAll(modeFormesUniesSimples, modeFormesUniesArrondies, modeImages);
        
        // Cr�ation du menu "Exemples" avec ses �l�ments
        Menu menuExemples = new Menu("Exemples");
        MenuItem exempleFormeUnieSimple = new MenuItem("Exemple Formes Unies Simples");
        exempleFormeUnieSimple.setOnAction(e -> ControleMode.changerVersModeFormesUniesSimples(true));
        MenuItem exempleFormeUnieArrondie = new MenuItem("Exemple Formes Unies Arrondies");
        exempleFormeUnieArrondie.setOnAction(e -> ControleMode.changerVersModeFormesUniesArrondies(true));
        MenuItem exempleImage = new MenuItem("Exemple Images");
        exempleImage.setOnAction(e -> ControleMode.changerVersModeImages(true));
        menuExemples.getItems().addAll(exempleFormeUnieSimple, exempleFormeUnieArrondie, exempleImage);
        
        // Cr�ation de l'�l�ment de menu "Param�tres" avec son action associ�e
        MenuItem parametres = new MenuItem("Param�tres");
        GridPane rootFenetreParametres = creerRootFenetreParametres();
        parametres.setOnAction(e -> afficherFenetreModale("Param�tres", rootFenetreParametres, false));
        
        // Cr�ation de l'�l�ment de menu "Quitter" avec son action associ�e
        MenuItem itemQuitter = new MenuItem("Quitter");
        itemQuitter.setOnAction(e -> {
        	if (InterfaceMorphing.afficherConfirmation("Quitter l'application ?", "�tes-vous s�r de vouloir quitter l'application ? Tout travail effectu� sera perdu.")) {
        		System.exit(0);
        	}
        });
        
        // Ajout des �l�ments au menu "Application"
        menuApplication.getItems().addAll(menuModes, menuExemples, parametres, new SeparatorMenuItem(), itemQuitter);
     
        // Cr�ation du menu "Images" avec ses �l�ments
        Menu menuImages = new Menu("Images");
        MenuItem ajouterImage = new MenuItem("Ajouter une image");
        ControleMenuItemAjoutOuModificationImages controleAjouterImage = new ControleMenuItemAjoutOuModificationImages(this, ajouterImage, ControleMenuItemAjoutOuModificationImages.MENU_ITEM_AJOUT_IMAGE);
        donneesAssociees.getListeObservateurs().add(controleAjouterImage);
        ajouterImage.setOnAction(controleAjouterImage);
        
        MenuItem modifierImageActuelle = new MenuItem("Modifier l'image actuelle");
        ControleMenuItemAjoutOuModificationImages controleModifierImageActuelle = new ControleMenuItemAjoutOuModificationImages(this, modifierImageActuelle, ControleMenuItemAjoutOuModificationImages.MENU_ITEM_MODIFICATION_IMAGE_ACTUELLE);
        donneesAssociees.getListeObservateurs().add(controleModifierImageActuelle);
        modifierImageActuelle.setOnAction(controleModifierImageActuelle);
        modifierImageActuelle.setDisable(true);
        
        MenuItem decalerImageActuelleVersDebut = new MenuItem("D�caler l'image actuelle vers le d�but");
        ControleMenuItemDecalageImageActuelle controleDecalerImageActuelleVersDebut = new ControleMenuItemDecalageImageActuelle(this, decalerImageActuelleVersDebut, ControleMenuItemDecalageImageActuelle.MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_DEBUT);
        donneesAssociees.getListeObservateurs().add(controleDecalerImageActuelleVersDebut);
        decalerImageActuelleVersDebut.setOnAction(controleDecalerImageActuelleVersDebut);
        decalerImageActuelleVersDebut.setDisable(true);
        
        MenuItem decalerImageActuelleVersFin = new MenuItem("D�caler l'image actuelle vers la fin");
        ControleMenuItemDecalageImageActuelle controleDecalerImageActuelleVersFin = new ControleMenuItemDecalageImageActuelle(this, decalerImageActuelleVersFin, ControleMenuItemDecalageImageActuelle.MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_FIN);
        donneesAssociees.getListeObservateurs().add(controleDecalerImageActuelleVersFin);
        decalerImageActuelleVersFin.setOnAction(controleDecalerImageActuelleVersFin);
        decalerImageActuelleVersFin.setDisable(true);
        
        MenuItem supprimerImageActuelle = new MenuItem("Supprimer l'image actuelle");
        ControleMenuItemSuppressionImages controleMenuItemSupprimerImage = new ControleMenuItemSuppressionImages(this, supprimerImageActuelle, ControleMenuItemSuppressionImages.MENU_ITEM_SUPPRESSION_IMAGE_ACTUELLE);
        donneesAssociees.getListeObservateurs().add(controleMenuItemSupprimerImage);
        supprimerImageActuelle.setOnAction(controleMenuItemSupprimerImage);
        supprimerImageActuelle.setDisable(true);
        
        MenuItem supprimerToutesLesImages = new MenuItem("Tout supprimer");
        ControleMenuItemSuppressionImages controleMenuItemSupprimerToutesLesImages = new ControleMenuItemSuppressionImages(this, supprimerToutesLesImages, ControleMenuItemSuppressionImages.MENU_ITEM_SUPPRESSION_TOUTES_LES_IMAGES);
        donneesAssociees.getListeObservateurs().add(controleMenuItemSupprimerToutesLesImages);
        supprimerToutesLesImages.setOnAction(controleMenuItemSupprimerToutesLesImages);
        supprimerToutesLesImages.setDisable(true);

        // Ajout des �l�ments au menu "Images"
        menuImages.getItems().addAll(ajouterImage, modifierImageActuelle, new SeparatorMenuItem(), decalerImageActuelleVersDebut, decalerImageActuelleVersFin, new SeparatorMenuItem(), supprimerImageActuelle, supprimerToutesLesImages);
        
        // Cr�ation du menu "Points De Contr�le" avec ses �l�ments
        Menu menuPointsDeControle = new Menu("Points De Contr�le");
        MenuItem supprimerLeDernierPointDeControle = new MenuItem("Supprimer le dernier point de contr�le ajout�");
        ControleMenuItemSuppressionPointsDeControle controleMenuItemSupprimerLeDernierPointDeControle = new ControleMenuItemSuppressionPointsDeControle(this, supprimerLeDernierPointDeControle, ControleMenuItemSuppressionPointsDeControle.MENU_ITEM_SUPPRESSION_DERNIER_POINT_DE_CONTROLE);
        donneesAssociees.getListeObservateurs().add(controleMenuItemSupprimerLeDernierPointDeControle);
        supprimerLeDernierPointDeControle.setOnAction(controleMenuItemSupprimerLeDernierPointDeControle);
        supprimerLeDernierPointDeControle.setDisable(true);
        
        MenuItem supprimerTousLesPointsDeControle = new MenuItem("Tout supprimer");
        ControleMenuItemSuppressionPointsDeControle controleMenuItemSupprimerTousLesPointsDeControle = new ControleMenuItemSuppressionPointsDeControle(this, supprimerTousLesPointsDeControle, ControleMenuItemSuppressionPointsDeControle.MENU_ITEM_SUPPRESSION_TOUTES_LES_POINT_DE_CONTROLE);
        donneesAssociees.getListeObservateurs().add(controleMenuItemSupprimerTousLesPointsDeControle);
        supprimerTousLesPointsDeControle.setOnAction(controleMenuItemSupprimerTousLesPointsDeControle);
        supprimerTousLesPointsDeControle.setDisable(true);
        
        MenuItem aidePointsDeControle = new MenuItem("Aide");
        aidePointsDeControle.setOnAction(e -> afficherAlerte(genererAidePlacerPointsDeControle()));
        
        // Ajout des �l�ments au menu "Points De Contr�le"
        menuPointsDeControle.getItems().addAll(supprimerLeDernierPointDeControle, supprimerTousLesPointsDeControle, new SeparatorMenuItem(), aidePointsDeControle);
        
        // Cr�ation de la barre de menu et ajout des menus cr��s
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(menuApplication, menuImages, menuPointsDeControle);
        return menuBar;
    }
    
    /**
     * Cr�e la racine de la fen�tre des param�tres de l'interface.
     * @return La racine de la fen�tre des param�tres.
     */
    private GridPane creerRootFenetreParametres() {
        // Cr�ation de la grille pour les param�tres
        GridPane rootFenetreParametres = new GridPane();
        rootFenetreParametres.setHgap(10);
        rootFenetreParametres.setVgap(10);
        rootFenetreParametres.setAlignment(javafx.geometry.Pos.CENTER);
        
        // D�finition des contraintes de colonnes
        ColumnConstraints contrainteColonne1 = new ColumnConstraints();
        ColumnConstraints contrainteColonne2 = new ColumnConstraints();
        contrainteColonne1.setPercentWidth(80);
        contrainteColonne2.setPercentWidth(20);
        rootFenetreParametres.getColumnConstraints().addAll(contrainteColonne1, contrainteColonne2);
        
        // Ajout du s�lecteur de couleur pour les points de contr�le
        colorPicker = new ColorPicker(Color.WHITE);
        colorPicker.setOnAction(event -> {
            Color nouvelleCouleur = colorPicker.getValue();
            setCouleurPointDeControle(nouvelleCouleur);
            mettreAJourAffichagePointsControle();
        });
        rootFenetreParametres.add(new StackPane(new Label("Couleur des points de contr�les :")), 0, 0);
        rootFenetreParametres.add(new StackPane(colorPicker), 1, 0);
        
        // Ajout du champ de texte pour le nombre d'images interm�diaires
        textFieldNombreImagesIntermediaires = new TextField("30");
        textFieldNombreImagesIntermediaires.setAlignment(javafx.geometry.Pos.CENTER);
        textFieldNombreImagesIntermediaires.textProperty().addListener((valeurObservable, ancienneValeur, nouvelleValeur) -> {
            if (nouvelleValeur.matches("\\d*")) {
                textFieldNombreImagesIntermediaires.setText(nouvelleValeur);
            } else {
                textFieldNombreImagesIntermediaires.setText(ancienneValeur);
            }
        });
        textFieldNombreImagesIntermediaires.focusedProperty().addListener((valeurObservable, ancienneValeur, nouvelleValeur) -> {
            if (!nouvelleValeur && !textFieldNombreImagesIntermediaires.getText().isEmpty()) {
                    int valeur = Integer.parseInt(textFieldNombreImagesIntermediaires.getText());
                    if (valeur >= 0 && valeur <= 200) {
                        textFieldNombreImagesIntermediaires.setText(Integer.toString(valeur));
                    } else {
                        textFieldNombreImagesIntermediaires.setText("30");
                    }
            } else if (!nouvelleValeur) {
                textFieldNombreImagesIntermediaires.setText("30");
            }
        });
        rootFenetreParametres.add(new StackPane(new Label("Nombre d'images interm�diaires entre chaque couple d'images (entre 0 et 200 inclus) :")), 0, 1);
        rootFenetreParametres.add(new StackPane(textFieldNombreImagesIntermediaires), 1, 1);
        
        // Ajout du champ de texte pour la dur�e du GIF
        textFieldDureeGIF = new TextField("1.0");
        textFieldDureeGIF.setAlignment(javafx.geometry.Pos.CENTER);
        textFieldDureeGIF.textProperty().addListener((valeurObservable, ancienneValeur, nouvelleValeur) -> {
            if (nouvelleValeur.matches("\\d*\\.?\\d*")) {
                textFieldDureeGIF.setText(nouvelleValeur);
            } else {
                textFieldDureeGIF.setText(ancienneValeur);
            }
        });
        textFieldDureeGIF.focusedProperty().addListener((valeurObservable, ancienneValeur, nouvelleValeur) -> {
            if (!nouvelleValeur && !textFieldDureeGIF.getText().isEmpty()) {
                    double valeur = Double.parseDouble(textFieldDureeGIF.getText());
                    if (valeur > 0 && valeur <= 20) {
                        textFieldDureeGIF.setText(Double.toString(valeur));
                    } else {
                        textFieldDureeGIF.setText("1.0");
                    }
            } else if (!nouvelleValeur) {
                textFieldDureeGIF.setText("1.0");
            }
        });
        rootFenetreParametres.add(new StackPane(new Label("Dur�e du GIF (en secondes, entre 0 exclu et 20 inclus) :")), 0, 2);
        rootFenetreParametres.add(new StackPane(textFieldDureeGIF), 1, 2);
        
        return rootFenetreParametres;
    }

    /**
     * G�n�re l'aide pour placer les points de contr�le dans l'interface.
     * @return L'aide pour placer les points de contr�le.
     */
    protected abstract String genererAidePlacerPointsDeControle();

    /**
     * Cr�e la partie centrale de l'interface.
     * @return La partie centrale de l'interface.
     */
    private GridPane creerPartieCentrale() {
        GridPane partieCentrale = new GridPane();
        
        // Ajout de la partie ListView � la colonne 0
        partieCentrale.add(creerPartieListeView(), 0, 0);
        ColumnConstraints contraintesColonnePartieListeView = new ColumnConstraints();
        contraintesColonnePartieListeView.setPercentWidth(15);
        partieCentrale.getColumnConstraints().add(contraintesColonnePartieListeView);
        
        // Ajout de la partie ImageView � la colonne 1
        partieCentrale.add(creerPartieImageCentrale(), 1, 0);
        ColumnConstraints contraintesColonnePartieImageCentrale = new ColumnConstraints();
        contraintesColonnePartieImageCentrale.setPercentWidth(85);
        partieCentrale.getColumnConstraints().add(contraintesColonnePartieImageCentrale);
        
        // D�finition des contraintes de la ligne
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPercentHeight(100);
        partieCentrale.getRowConstraints().add(rowConstraints);
        
        return partieCentrale;
    }
    
    /**
     * Cr�e la partie ListView de l'interface.
     * @return La partie ListView de l'interface.
     */
    private ListView<String> creerPartieListeView() {
        ListView<String> listView = new ListView<>();
        ControleListView controleListView = new ControleListView(this, listView);
        listView.setOnMouseClicked(controleListView);
        listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        Label placeHolder = new Label("Charger une image pour continuer");
        placeHolder.setWrapText(true);
        listView.setPlaceholder(placeHolder);
        getDonneesAssociees().getListeObservateurs().add(controleListView);
        return listView;
    }
	
    /**
     * Cr�e la partie ImageView de l'interface.
     * @return La partie ImageView de l'interface.
     */
    private BorderPane creerPartieImageCentrale() {        
        Pane boiteImage = new Pane();
        ControleChangementTailleBoiteImage controleChangementDeTailleBoiteImage = new ControleChangementTailleBoiteImage(this, boiteImage);
        boiteImage.widthProperty().addListener(controleChangementDeTailleBoiteImage);
        boiteImage.heightProperty().addListener(controleChangementDeTailleBoiteImage);
        // Afin d'�viter que l'image ait une taille de 0, on met un minimum de 1 pixel
        boiteImage.setMinWidth(1);
        boiteImage.setMinHeight(1);
        ajouterProprieteCliquableABoiteImage(boiteImage);
        
        ImageView imageView = new ImageView();
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(boiteImage.widthProperty());
        imageView.fitHeightProperty().bind(boiteImage.heightProperty());
        boiteImage.getChildren().add(imageView);
        
        Label legendeLabel = new Label("Image actuelle");
        legendeLabel.setPadding(new Insets(10, 10, 0, 10));
        legendeLabel.getStyleClass().add("legende");
        
        BorderPane partieImageCentrale = new BorderPane();
        partieImageCentrale.setCenter(boiteImage);
        partieImageCentrale.setBottom(new StackPane(legendeLabel));
        partieImageCentrale.setPadding(new Insets(0, 0, 0, 20));
        
        return partieImageCentrale;
    }
	
    /**
     * Met � jour l'affichage de l'image centrale dans l'interface.
     */
    public void mettreAJourAffichageImageCentrale() {
        ImageView imageView = getImageView(getBoiteImageCentrale());
        if (donneesAssociees.uneImageEstSelectionnee()) {
            imageView.setImage(getDonneesAssociees().getImageActuelle());
            imageView.setTranslateX(getTranslationXImageDansBoiteImage(getBoiteImageCentrale()));
            imageView.setTranslateY(getTranslationYImageDansBoiteImage(getBoiteImageCentrale()));
        } else {
            imageView.setImage(null);
        }
    }
    
    /**
     * Cr�e et ajoute des propri�t�s cliquables � la bo�te d'image sp�cifi�e.
     * @param boiteImage La bo�te d'image � laquelle ajouter des propri�t�s cliquables.
     */
    protected abstract void ajouterProprieteCliquableABoiteImage(Pane boiteImage);
    
    /**
     * Cr�e le bandeau inf�rieur de l'interface.
     * @return Le bandeau inf�rieur de l'interface.
     */
    private HBox creerBandeauBas() {
        Button boutonGenererMorphing = new Button("G�n�rer le morphing");
        ControleBoutonGenererMorphing controleBoutonGenererMorphing = creerControleBoutonGenererMorphing(boutonGenererMorphing);
        getDonneesAssociees().getListeObservateurs().add(controleBoutonGenererMorphing);
        boutonGenererMorphing.setOnAction(controleBoutonGenererMorphing);
        boutonGenererMorphing.setDisable(true);
        
        HBox bandeauBas = new HBox(new StackPane(boutonGenererMorphing), new StackPane());
        bandeauBas.setAlignment(javafx.geometry.Pos.CENTER);
        
        return bandeauBas;
    }

    /**
     * Cr�e un contr�le pour le bouton de g�n�ration de morphing.
     * @param boutonGenererMorphing Le bouton de g�n�ration de morphing auquel attacher le contr�le.
     * @return Le contr�le pour le bouton de g�n�ration de morphing.
     */
    protected abstract ControleBoutonGenererMorphing creerControleBoutonGenererMorphing(Button boutonGenererMorphing);

    /**
     * G�n�re la sc�ne de l'interface.
     * @return La sc�ne de l'interface.
     */
    private Scene genererScene() {
        root.setTop(bandeauHaut);
        root.setCenter(partieCentrale);
        root.setBottom(bandeauBas);

        BorderPane.setMargin(partieCentrale, new Insets(20));
        BorderPane.setMargin(bandeauBas, new Insets(0, 20, 20, 20));

        Scene scene = new Scene(root, 1100, 600);
        scene.getStylesheets().add("/ressources/css/interface.css");
        return scene;
    }
    
    /**
     * M�thode abstraite pour mettre � jour l'affichage des points de contr�le.
     */
    public abstract void mettreAJourAffichagePointsControle();

    /**
     * Cr�e un groupe repr�sentant un point de contr�le avec une �tiquette.
     * @param x Coordonn�e x du point de contr�le.
     * @param y Coordonn�e y du point de contr�le.
     * @param etiquette �tiquette associ�e au point de contr�le.
     * @return Le groupe repr�sentant le point de contr�le.
     */
    private Group creerUnNoeudPointControle(double x, double y, String etiquette) {
        // Cr�e un cercle pour repr�senter le point de contr�le
        Circle cercleExterieur = new Circle(5);
        cercleExterieur.setFill(Color.TRANSPARENT);
        cercleExterieur.setStroke(colorPicker.getValue());
        cercleExterieur.setStrokeWidth(2);
        
        // Cr�e des lignes pour repr�senter les axes du point de contr�le
        Line ligneHorizontale = new Line(-2.5, 0, 2.5, 0);
        ligneHorizontale.setStroke(getCouleurPointDeControle());
        ligneHorizontale.setStrokeWidth(1);
        Line ligneVerticale = new Line(0, -2.5, 0, 2.5);
        ligneVerticale.setStroke(getCouleurPointDeControle());
        ligneVerticale.setStrokeWidth(1);
        
        // Cr�e un texte pour afficher l'�tiquette du point de contr�le
        Text texteNumero = new Text(etiquette);
        texteNumero.setFill(getCouleurPointDeControle());
        texteNumero.setTranslateX(4);
        texteNumero.setTranslateY(-4);
        texteNumero.setFont(Font.font("Arial", FontWeight.BOLD, 10));
        
        // Cr�e un groupe pour regrouper les �l�ments du point de contr�le
        Group pointDeControle = new Group(cercleExterieur, ligneHorizontale, ligneVerticale, texteNumero);
        pointDeControle.setLayoutX(x);
        pointDeControle.setLayoutY(y);
        pointDeControle.getStyleClass().add("afficherSourisClic");
        
        return pointDeControle;
    }

    /**
     * Cr�e un groupe repr�sentant un point de contr�le dans une bo�te image.
     * @param boiteImage La bo�te image o� le point de contr�le doit �tre cr��.
     * @param facteurDeZoomImage Le facteur de zoom de l'image.
     * @param pointDeControleSimple Le point de contr�le simple � repr�senter.
     * @param etiquette L'�tiquette associ�e au point de contr�le.
     * @param doitEtreDeplacable Indique si le point de contr�le doit �tre d�pla�able.
     * @return Le groupe repr�sentant le point de contr�le dans la bo�te image.
     */
    public Group creerUnNoeudPointControleDansBoiteImage(Pane boiteImage, double facteurDeZoomImage, PointControleSimple pointDeControleSimple, String etiquette, Boolean doitEtreDeplacable) {
        if (boiteImageContientUneImage(boiteImage)) {
            // Calcule les coordonn�es du point de contr�le dans la bo�te image
            double x = (pointDeControleSimple.getX() * facteurDeZoomImage) + getTranslationXImageDansBoiteImage(boiteImage);
            double y = (pointDeControleSimple.getY() * facteurDeZoomImage) + getTranslationYImageDansBoiteImage(boiteImage);
            
            // Cr�e un nouveau n�ud de point de contr�le
            Group noeudPointDeControle = creerUnNoeudPointControle(x, y, etiquette);
            
            if (doitEtreDeplacable) {
                // Rend le n�ud de point de contr�le d�pla�able
                CreateurNoeudPointControleDeplacableDansImageView createurDeNoeudDeplacable = new CreateurNoeudPointControleDeplacableDansImageView(getImageView(boiteImage));
                createurDeNoeudDeplacable.rendreNoeudPointControleDeplacable(noeudPointDeControle);
            }
            
            return noeudPointDeControle;
        } else {
            return null;
        }
    }

    /**
     * Cr�e une ligne reliant deux n�uds.
     * @param noeud1 Le premier n�ud.
     * @param noeud2 Le deuxi�me n�ud.
     * @return La ligne reliant les deux n�uds.
     */
    protected Line creerLigneEntreDeuxNoeuds(Node noeud1, Node noeud2) {
        // Cr�e une ligne reliant les deux n�uds
        Line ligne = new Line(noeud1.getLayoutX() + noeud1.getTranslateX(), noeud1.getLayoutY() + noeud1.getTranslateY(), noeud2.getLayoutX() + noeud2.getTranslateX(), noeud2.getLayoutY() + noeud2.getTranslateY());
        ligne.setStroke(getCouleurPointDeControle());
        ligne.setStrokeWidth(1);
        ligne.setPickOnBounds(false);
        return ligne;
    }

    /**
     * Cr�e une courbe cubique reliant quatre n�uds.
     * @param noeud1 Premier n�ud.
     * @param noeud2 Deuxi�me n�ud.
     * @param noeud3 Troisi�me n�ud.
     * @param noeud4 Quatri�me n�ud.
     * @return La courbe reliant les quatre n�uds.
     */
    protected CubicCurve creerCourbeEntreDeuxNoeuds(Node noeud1, Node noeud2, Node noeud3, Node noeud4) {
        // R�cup�re les coordonn�es des n�uds
        double x1 = noeud1.getLayoutX();
        double y1 = noeud1.getLayoutY();
        double x2 = noeud2.getLayoutX();
        double y2 = noeud2.getLayoutY();
        double x3 = noeud3.getLayoutX();
        double y3 = noeud3.getLayoutY();
        double x4 = noeud4.getLayoutX();
        double y4 = noeud4.getLayoutY();
        
        // Cr�e une courbe cubique reliant les quatre n�uds
        CubicCurve courbe = new CubicCurve();
        courbe.setStartX(x1);
        courbe.setStartY(y1);
        courbe.setControlX1(x2);
        courbe.setControlY1(y2);
        courbe.setControlX2(x3);
        courbe.setControlY2(y3);
        courbe.setEndX(x4);
        courbe.setEndY(y4);
        
        courbe.setStroke(getCouleurPointDeControle());
        courbe.setStrokeWidth(1);
        courbe.setFill(Color.TRANSPARENT);
        return courbe;
    }
    
    /**
     * Affiche une alerte avec un message.
     * @param message Le message � afficher.
     */
    public static void afficherAlerte(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Informations");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.setWidth(570);
        alert.setHeight(320);
        alert.setResizable(true);
        alert.showAndWait();
    }

    /**
     * Affiche une bo�te de confirmation avec un titre et un message.
     * @param titre Le titre de la bo�te de confirmation.
     * @param message Le message � afficher.
     * @return True si l'utilisateur a confirm�, sinon False.
     */
    public static boolean afficherConfirmation(String titre, String message) {
        Alert confirmation = new Alert(AlertType.CONFIRMATION);
        confirmation.setTitle("Confirmation");
        confirmation.setHeaderText(titre);
        confirmation.setContentText(message);
        confirmation.setWidth(400);
        confirmation.setHeight(230);
        confirmation.setResizable(true);
        return confirmation.showAndWait().orElse(ButtonType.CANCEL) == ButtonType.OK;
    }

    /**
     * Affiche une fen�tre modale avec un titre et une partie centrale.
     * @param titre Le titre de la fen�tre modale.
     * @param partieCentrale La partie centrale de la fen�tre modale.
     * @param doitEtreIndependante Indique si la fen�tre modale doit �tre ind�pendante.
     */
    public static void afficherFenetreModale(String titre, Node partieCentrale, Boolean doitEtreIndependante) {
        Stage fenetreModale = new Stage();
        fenetreModale.initModality(doitEtreIndependante ? javafx.stage.Modality.WINDOW_MODAL : javafx.stage.Modality.APPLICATION_MODAL);
        fenetreModale.setTitle(titre);
        
        BorderPane root = new BorderPane();
        root.setCenter(partieCentrale);

        Button boutonFermer = new Button("Fermer");
        boutonFermer.setOnAction(e -> fenetreModale.close());
        StackPane bandeauBas = new StackPane(boutonFermer);
        root.setBottom(bandeauBas);
        
        BorderPane.setMargin(partieCentrale, new Insets(20, 20, 20, 20));
        BorderPane.setMargin(bandeauBas, new Insets(0, 20, 20, 20));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/ressources/css/interface.css");
        fenetreModale.setScene(scene);
        fenetreModale.setResizable(true);
        fenetreModale.getIcons().add(new Image("/ressources/images/logo.png"));
        fenetreModale.show();
    }

    /**
     * V�rifie si les coordonn�es (sceneX, sceneY) sont � l'int�rieur du n�ud sp�cifi�.
     * @param noeud Le n�ud dans lequel v�rifier les coordonn�es.
     * @param sceneX La coordonn�e x dans la sc�ne.
     * @param sceneY La coordonn�e y dans la sc�ne.
     * @return True si les coordonn�es sont � l'int�rieur du n�ud, sinon False.
     */
    public static boolean coordonneesDansNode(Node noeud, double sceneX, double sceneY) {
        if (noeud.getScene() != null) {
            double localX = noeud.sceneToLocal(sceneX, sceneY).getX();
            double localY = noeud.sceneToLocal(sceneX, sceneY).getY();
            return localX >= 0 && localX <= noeud.getBoundsInLocal().getWidth() && localY >= 0 && localY <= noeud.getBoundsInLocal().getHeight();
        } else {
            return false;
        }
    }
    
    /**
     * V�rifie si une image est affich�e dans la bo�te d'image centrale.
     * @return True si une image est affich�e, sinon False.
     */
    public Boolean uneImageEstAffichee() {
        return boiteImageContientUneImage(getBoiteImageCentrale());
    }

    /**
     * Classe interne pour rendre un n�ud de point de contr�le d�pla�able dans une ImageView.
     */
    public class CreateurNoeudPointControleDeplacableDansImageView {
        private Node noeudLimite;
        private double ancreX;
        private double ancreY;

        public CreateurNoeudPointControleDeplacableDansImageView(ImageView imageView) {
            this.noeudLimite = imageView;
        }

        public void rendreNoeudPointControleDeplacable(Group noeudPointControle) {
            // G�re l'�v�nement de pression de la souris
            noeudPointControle.setOnMousePressed(e -> {
                ancreX = e.getSceneX() - noeudPointControle.getLayoutX();
                ancreY = e.getSceneY() - noeudPointControle.getLayoutY();
            });

            // G�re l'�v�nement de glissement de la souris
            noeudPointControle.setOnMouseDragged(e -> {
                double nouveauLayoutX = e.getSceneX() - ancreX;
                double nouveauLayoutY = e.getSceneY() - ancreY;

                // V�rifie si les nouvelles coordonn�es sont dans la limite de la ImageView
                if (coordonneesDansNode(noeudLimite, noeudPointControle.getParent().localToScene(nouveauLayoutX, nouveauLayoutY).getX(), noeudPointControle.getParent().localToScene(nouveauLayoutX, nouveauLayoutY).getY())) {
                    noeudPointControle.setLayoutX(nouveauLayoutX);
                    noeudPointControle.setLayoutY(nouveauLayoutY);
                }

                // Met � jour l'affichage des lignes entre les points de contr�le
                mettreAJourAffichageLignesEntrePointsDeControle((Pane) ((Group) e.getSource()).getParent());

                // Indique qu'un n�ud de point de contr�le a �t� d�plac�
                unNoeudPointControleVientDEtreDeplace(true);
            });

            // G�re l'�v�nement de rel�chement de la souris
            noeudPointControle.setOnMouseReleased(e -> {
                if (unNoeudPointControleVientDEtreDeplace()) {
                    double translationX = InterfaceMorphing.getTranslationXImageDansBoiteImage((Pane) ((Group) e.getSource()).getParent());
                    double translationY = InterfaceMorphing.getTranslationYImageDansBoiteImage((Pane) ((Group) e.getSource()).getParent());

                    // Met � jour les coordonn�es du point de contr�le dans l'image centrale
                    mettreAJourPointControleImageCentrale(((Text) noeudPointControle.getChildren().get(3)).getText(), (noeudPointControle.getLayoutX() - translationX) / getFacteurDeZoomDesImagesAffichees(), (noeudPointControle.getLayoutY() - translationY) / getFacteurDeZoomDesImagesAffichees());
                }
                // R�initialise le flag indiquant le d�placement d'un n�ud de point de contr�le
                unNoeudPointControleVientDEtreDeplace(false);
            });
        }
    }

    /**
     * M�thode abstraite pour mettre � jour l'affichage des lignes entre les points de contr�le.
     * @param boiteImage La bo�te d'image.
     */
    protected abstract void mettreAJourAffichageLignesEntrePointsDeControle(Pane boiteImage);

    /**
     * M�thode abstraite pour mettre � jour les coordonn�es du point de contr�le dans l'image centrale.
     * @param etiquette L'�tiquette du point de contr�le.
     * @param x La coordonn�e x du point de contr�le.
     * @param y La coordonn�e y du point de contr�le.
     */
    protected abstract void mettreAJourPointControleImageCentrale(String etiquette, double x, double y);

    /**
     * D�sactive les �v�nements pour une liste de n�uds.
     * @param nodes La liste des n�uds � d�sactiver.
     */
    public static void desactiverEvenements(List<Node> nodes) {
        for (Node node : nodes) {
            node.setDisable(true);
        }
    }

    /**
     * D�sactive les �v�nements pour une liste variable de n�uds.
     * @param nodes Les n�uds � d�sactiver.
     */
    public static void desactiverEvenements(Node... nodes) {
        for (Node node : nodes) {
            node.setDisable(true);
        }
    }

    /**
     * R�active les �v�nements pour une liste de n�uds.
     * @param nodes La liste des n�uds � r�activer.
     */
    public static void reactiverEvenements(List<Node> nodes) {
        for (Node node : nodes) {
            node.setDisable(false);
        }
    }
    
    /**
     * R�active les �v�nements pour une liste variable de n�uds.
     * @param nodes N variables de n�uds.
     */
    public static void reactiverEvenements(Node... nodes) {
        for (Node node : nodes) {
            node.setDisable(false);
        }
    }
    
    /**
     * Ex�cute une t�che longue de mani�re asynchrone.
     * @param tacheAEffectuer La t�che � ex�cuter.
     * @param instructionAFaireApresTache L'instruction � ex�cuter apr�s la t�che.
     * @param boiteProgressIndicator La bo�te d'indicateur de progression.
     */
    public static void executerLongueTache(Runnable tacheAEffectuer, Runnable instructionAFaireApresTache, StackPane boiteProgressIndicator) {
        Task<Void> longTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                tacheAEffectuer.run();
                return null;
            }
        };

        // G�re l'�v�nement de r�ussite de la t�che
        longTask.setOnSucceeded(event -> {
            boiteProgressIndicator.getChildren().clear();
            instructionAFaireApresTache.run();
        });

        // G�re l'�v�nement d'�chec de la t�che
        longTask.setOnFailed(event -> {
            boiteProgressIndicator.getChildren().clear();
            Throwable exception = longTask.getException();
            exception.printStackTrace();
        });
        
        // Affiche l'indicateur de progression
        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxHeight(boiteProgressIndicator.getHeight());
        boiteProgressIndicator.getChildren().clear();
        boiteProgressIndicator.getChildren().add(progressIndicator);
        Thread thread = new Thread(longTask);
        thread.setDaemon(true);
        thread.start();
    }
}