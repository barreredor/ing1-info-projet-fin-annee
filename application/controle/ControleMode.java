package application.controle;

import application.Main;
import application.abstraction.PointControleDouble;
import application.abstraction.PointControleSimple;
import application.presentation.*;

/**
 * Cette classe abstraite contient des m�thodes statiques pour changer le mode de l'interface.
 */
public abstract class ControleMode {
	
    /**
     * M�thode pour changer vers le mode Formes Unies Simples.
     * @param chargerAussiExemple Indique si l'exemple doit �tre charg�.
     */
    public static void changerVersModeFormesUniesSimples(Boolean chargerAussiExemple) {
        if (InterfaceMorphing.afficherConfirmation("Ouvrir un nouvel onglet ?", "�tes-vous s�r de vouloir ouvrir un nouvel onglet avec le mode Formes Unies Simples ? Vous perdrez tout ce que vous avez fait jusqu'� pr�sent.")) {
        	InterfaceMorphingFormesUniesSimples interfaceMorphing = new InterfaceMorphingFormesUniesSimples();
        	Main.stage.setScene(interfaceMorphing.getScene());
        	
        	if (chargerAussiExemple) {
        		chargerJeuDeDonneesMorphing(interfaceMorphing);
        	}
        }
    }

    /**
     * M�thode pour changer vers le mode Formes Unies Arrondies.
     * @param chargerAussiExemple Indique si l'exemple doit �tre charg�.
     */
    public static void changerVersModeFormesUniesArrondies(Boolean chargerAussiExemple) {
    	if (InterfaceMorphing.afficherConfirmation("Ouvrir un nouvel onglet ?", "�tes-vous s�r de vouloir ouvrir un nouvel onglet avec le mode Formes Unies Arrondies ? Vous perdrez tout ce que vous avez fait jusqu'� pr�sent.")) {
	        InterfaceMorphingFormesUniesArrondies interfaceMorphing = new InterfaceMorphingFormesUniesArrondies();
	    	Main.stage.setScene(interfaceMorphing.getScene());
	    	
	    	if (chargerAussiExemple) {
	    		chargerJeuDeDonneesMorphing(interfaceMorphing);
	    	}
    	}
    }

    /**
     * M�thode pour changer vers le mode Images.
     * @param chargerAussiExemple Indique si l'exemple doit �tre charg�.
     */
    public static void changerVersModeImages(Boolean chargerAussiExemple) {
    	if (InterfaceMorphing.afficherConfirmation("Ouvrir un nouvel onglet ?", "�tes-vous s�r de vouloir ouvrir un nouvel onglet avec le mode Images ? Vous perdrez tout ce que vous avez fait jusqu'� pr�sent.")) {
	        InterfaceMorphingImages interfaceMorphing = new InterfaceMorphingImages();
	    	Main.stage.setScene(interfaceMorphing.getScene());
	    	
	    	if (chargerAussiExemple) {
	    		chargerJeuDeDonneesMorphing(interfaceMorphing);
	    	}
    	}
    }
    
    /**
     * M�thode priv�e pour charger un exemple de jeu de donn�es pour le mode Formes Unies Simples.
     * @param interfaceMorphing L'interface morphing � charger.
     */
    private static void chargerJeuDeDonneesMorphing(InterfaceMorphingFormesUniesSimples interfaceMorphing) {
	    interfaceMorphing.getDonneesAssociees().ajouterImage("/ressources/images/morphingFormesUniesSimples_depart.png");
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(132, 162), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(132, 854), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(883, 854), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(883, 162), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(132, 162), 0);

	    interfaceMorphing.getDonneesAssociees().ajouterImage("/ressources/images/morphingFormesUniesSimples_arrivee.png");
	    interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(0, new PointControleSimple(317, 447), 1);
	    interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(1, new PointControleSimple(317, 734), 1);
	    interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(2, new PointControleSimple(602, 734), 1);
	    interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(3, new PointControleSimple(602, 447), 1);
	    interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(4, new PointControleSimple(317, 447), 1);
	}
	
    /**
     * M�thode priv�e pour charger un exemple de jeu de donn�es pour le mode Formes Unies Arrondies.
     * @param interfaceMorphing L'interface morphing � charger.
     */
	private static void chargerJeuDeDonneesMorphing(InterfaceMorphingFormesUniesArrondies interfaceMorphing) {
		interfaceMorphing.getDonneesAssociees().ajouterImage("/ressources/images/morphingFormesUniesArrondies_depart.png");
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(109, 484), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(122, 607), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(140, 672), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(207, 755), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(305, 857), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(392, 875), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(500, 884), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(610, 867), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(687, 844), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(768, 767), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(822, 699), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(878, 595), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(872, 497), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(864, 397), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(835, 314), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(747, 220), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(662, 164), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(610, 139), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(512, 125), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(406, 131), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(354, 154), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(265, 197), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(159, 279), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(126, 397), 0);
	    interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple(109, 484), 0);
	    
		interfaceMorphing.getDonneesAssociees().ajouterImage("/ressources/images/morphingFormesUniesArrondies_arrivee.png");
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(0, new PointControleSimple(93, 486), 0);
	    interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(1, new PointControleSimple(86, 644), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(2, new PointControleSimple(280, 595), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(3, new PointControleSimple(406, 605), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(4, new PointControleSimple(413, 723), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(5, new PointControleSimple(367, 925), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(6, new PointControleSimple(500, 927), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(7, new PointControleSimple(641, 948), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(8, new PointControleSimple(608, 730), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(9, new PointControleSimple(614, 605), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(10, new PointControleSimple(739, 597), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(11, new PointControleSimple(918, 638), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(12, new PointControleSimple(939, 501), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(13, new PointControleSimple(922, 351), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(14, new PointControleSimple(731, 410), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(15, new PointControleSimple(612, 399), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(16, new PointControleSimple(598, 281), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(17, new PointControleSimple(652, 94), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(18, new PointControleSimple(512, 83), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(19, new PointControleSimple(365, 87), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(20, new PointControleSimple(411, 274), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(21, new PointControleSimple(404, 395), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(22, new PointControleSimple(282, 401), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(23, new PointControleSimple(107, 372), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(24, new PointControleSimple(93, 486), 0);
	}

	/**
     * M�thode priv�e pour charger un exemple de jeu de donn�es pour le mode Images.
     * @param interfaceMorphing L'interface morphing � charger.
     */
	private static void chargerJeuDeDonneesMorphing(InterfaceMorphingImages interfaceMorphing) {
		interfaceMorphing.getDonneesAssociees().ajouterImage("/ressources/images/morphingImages_depart.png");
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(103, 74, 164, 67), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(172, 74, 199, 131), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(199, 149, 194, 200), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(189, 209, 152, 247), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(137, 249, 92, 222), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(85, 213, 68, 137), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(91, 86, 72, 120), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(54, 137, 62, 180), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(218, 128, 211, 176), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(191, 246, 265, 291), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(82, 249, 2, 293), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(64, 132, 53, 75), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(83, 33, 56, 66), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(95, 26, 164, 22), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(176, 27, 209, 59), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(208, 70, 199, 136), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(81, 125, 115, 123), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(149, 123, 183, 120), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(151, 139, 177, 134), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(115, 140, 90, 139), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(116, 202, 164, 200), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(116, 202, 140, 188), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(140, 188, 164, 200), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(116, 202, 140, 209), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(140, 209, 164, 200), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(135, 123, 137, 175), 0);
		interfaceMorphing.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(121, 173, 151, 171), 0);
		
		interfaceMorphing.getDonneesAssociees().ajouterImage("/ressources/images/morphingImages_arrivee.jpg");
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(0, new PointControleDouble(76, 85, 166, 77), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(1, new PointControleDouble(170, 86, 197, 149), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(2, new PointControleDouble(198, 160, 197, 220), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(3, new PointControleDouble(192, 229, 154, 273), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(4, new PointControleDouble(132, 280, 76, 235), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(5, new PointControleDouble(73, 221, 65, 158), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(6, new PointControleDouble(73, 94, 64, 147), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(7, new PointControleDouble(50, 161, 61, 200), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(8, new PointControleDouble(211, 136, 210, 183), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(9, new PointControleDouble(200, 244, 263, 281), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(10, new PointControleDouble(65, 252, 2, 292), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(11, new PointControleDouble(52, 146, 37, 88), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(12, new PointControleDouble(78, 27, 40, 79), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(13, new PointControleDouble(91, 27, 160, 24), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(14, new PointControleDouble(176, 27, 206, 87), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(15, new PointControleDouble(207, 101, 199, 135), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(16, new PointControleDouble(81, 139, 114, 139), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(17, new PointControleDouble(140, 136, 178, 127), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(18, new PointControleDouble(145, 147, 172, 143), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(19, new PointControleDouble(112, 151, 84, 153), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(20, new PointControleDouble(117, 224, 161, 219), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(21, new PointControleDouble(117, 219, 130, 205), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(22, new PointControleDouble(136, 204, 159, 210), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(23, new PointControleDouble(119, 229, 136, 241), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(24, new PointControleDouble(140, 242, 162, 225), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(25, new PointControleDouble(127, 139, 132, 190), 0);
		interfaceMorphing.getDonneesAssociees().setPointControleImageActuelle(26, new PointControleDouble(119, 188, 149, 185), 0);
	}
}
