package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;

/**
 * Cette classe d�finit le contr�leur pour le changement des points de contr�le.
 */
public final class ControleChangementPointsDeControle extends Observateur {
    private InterfaceMorphing<?> interfaceAssociee;

    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     */
    public ControleChangementPointsDeControle(InterfaceMorphing<?> interfaceAssociee) {
        this.interfaceAssociee = interfaceAssociee;
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_POINTS_DE_CONTROLE) {
            interfaceAssociee.mettreAJourAffichagePointsControle();
        }
    }
}