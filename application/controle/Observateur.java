package application.controle;

import application.abstraction.Observable;

/**
 * Cette classe repr�sente un observateur qui r�agit aux modifications des observables.
 */
public abstract class Observateur {
    
    /**
     * M�thode abstraite appel�e pour traiter les modifications apr�s qu'elles se soient produites.
     * @param observable L'observable qui a �t� modifi�.
     * @param typeDeModification Le type de modification.
     */
    public abstract void traitementApresModification(Observable observable, Integer typeDeModification);
}