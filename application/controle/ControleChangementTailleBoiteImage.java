package application.controle;

import application.presentation.InterfaceMorphing;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * Cette classe d�finit le contr�leur pour le changement de taille de la bo�te d'image.
 */
public class ControleChangementTailleBoiteImage implements ChangeListener<Number> {
    private InterfaceMorphing<?> interfaceAssociee;
    private Pane boiteImage;
    
    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     * @param boiteImage La bo�te d'image � contr�ler.
     */
    public ControleChangementTailleBoiteImage(InterfaceMorphing<?> interfaceAssociee, Pane boiteImage) {
        this.interfaceAssociee = interfaceAssociee;
        this.boiteImage = boiteImage;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> valeurObservable, Number ancienneValeur, Number nouvelleValeur) {
        if (InterfaceMorphing.boiteImageContientUneImage(boiteImage)) {
            ImageView imageView = InterfaceMorphing.getImageView(boiteImage);
            imageView.setTranslateX(InterfaceMorphing.getTranslationXImageDansBoiteImage(boiteImage));
            imageView.setTranslateY(InterfaceMorphing.getTranslationYImageDansBoiteImage(boiteImage));        
            if (interfaceAssociee != null) {
                if (interfaceAssociee.uneImageEstAffichee()) {
                    interfaceAssociee.setFacteurZoomDeImageAffichee();
                }
                
                interfaceAssociee.mettreAJourAffichagePointsControle();
            }
        }
    }
}