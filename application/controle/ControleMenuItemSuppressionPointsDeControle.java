package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 * Cette classe d�finit le contr�leur pour la suppression des points de contr�le.
 */
public final class ControleMenuItemSuppressionPointsDeControle extends Observateur implements EventHandler<ActionEvent> {
    public static final Integer MENU_ITEM_SUPPRESSION_DERNIER_POINT_DE_CONTROLE = 1;
    public static final Integer MENU_ITEM_SUPPRESSION_TOUTES_LES_POINT_DE_CONTROLE = 2;
    
    private InterfaceMorphing<?> interfaceAssociee;
    private MenuItem menuItem;
    private Integer typeDeMenuItem;
    
    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     * @param menuItem Le MenuItem � contr�ler.
     * @param typeDeMenuItem Le type de MenuItem.
     */
    public ControleMenuItemSuppressionPointsDeControle(InterfaceMorphing<?> interfaceAssociee, MenuItem menuItem, Integer typeDeMenuItem) {
        this.interfaceAssociee = interfaceAssociee;
        this.menuItem = menuItem;
        this.typeDeMenuItem = typeDeMenuItem;
    }
    
    @Override
    public void handle(ActionEvent event) {
        if (typeDeMenuItem == MENU_ITEM_SUPPRESSION_DERNIER_POINT_DE_CONTROLE) {
            interfaceAssociee.getDonneesAssociees().supprimerPointControle(interfaceAssociee.getDonneesAssociees().getNombrePointsControle()-1);
        } else if (typeDeMenuItem == MENU_ITEM_SUPPRESSION_TOUTES_LES_POINT_DE_CONTROLE && InterfaceMorphing.afficherConfirmation("Supprimer tous les points de contr�le ?", "�tes-vous s�r de vouloir supprimer tous les points de contr�le ?")) {
            interfaceAssociee.getDonneesAssociees().supprimerTousLesPointsControle();
        }
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_POINTS_DE_CONTROLE) {
            menuItem.setDisable(interfaceAssociee.getDonneesAssociees().getNombrePointsControle() == 0);
        }
    }
}