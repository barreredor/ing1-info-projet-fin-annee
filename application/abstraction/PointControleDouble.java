package application.abstraction;

/**
 * Classe repr�sentant un point de contr�le double d�fini par deux points de contr�le simples.
 */
public final class PointControleDouble implements PointControle {
    private PointControleSimple point1;
    private PointControleSimple point2;

    /**
     * Constructeur de classe.
     * @param x1 Coordonn�e en abscisses du premier point.
     * @param y1 Coordonn�e en ordonn�es du premier point.
     * @param x2 Coordonn�e en abscisses du deuxi�me point.
     * @param y2 Coordonn�e en ordonn�es du deuxi�me point.
     */
    public PointControleDouble(double x1, double y1, double x2, double y2) {
        point1 = new PointControleSimple(x1, y1);
        point2 = new PointControleSimple(x2, y2);
    }

    /**
     * Renvoie le premier point de contr�le.
     * @return Le premier point de contr�le.
     */
    public PointControleSimple getPoint1() {
        return point1;
    }

    /**
     * Renvoie le deuxi�me point de contr�le.
     * @return Le deuxi�me point de contr�le.
     */
    public PointControleSimple getPoint2() {
        return point2;
    }

    public double getX1() {
        return point1.getX();
    }

    public double getY1() {
        return point1.getY();
    }

    public double getX2() {
        return point2.getX();
    }

    public double getY2() {
        return point2.getY();
    }

    public void setX1(double x1) {
        point1.setX(x1);
    }

    public void setY1(double y1) {
        point1.setY(y1);
    }

    public void setX2(double x2) {
        point2.setX(x2);
    }

    public void setY2(double y2) {
        point2.setY(y2);
    }

    @Override
    public PointControle copie() {
        return new PointControleDouble(point1.getX(), point1.getY(), point2.getX(), point2.getY());
    }

    @Override
    public void harmoniser(PointControle point, double seuilEquivalence) {
        if (point instanceof PointControleDouble) {
            PointControleDouble pointAHarmoniser = (PointControleDouble) point;

            point1.harmoniser(pointAHarmoniser.point1, seuilEquivalence);
            point1.harmoniser(pointAHarmoniser.point2, seuilEquivalence);
            point2.harmoniser(pointAHarmoniser.point1, seuilEquivalence);
            point2.harmoniser(pointAHarmoniser.point2, seuilEquivalence);
        }
    }

    @Override
    public String toString() {
        return "PointDeControleDouble{point1=(" + getX1() + ", " + getY1() + "), point2=(" + getX2() + ", " + getY2() + ")}";
    }

    /**
     * Calcule la norme du vecteur d�fini par les deux points de contr�le.
     * @return La norme du vecteur.
     */
    public double getNorme() {
        double x = this.getX2() - this.getX1();
        double y = this.getY2() - this.getY1();

        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }
    
	/**
	 * Calcule la norme u pr�sente dans l'algorithme de Beier et Neely.
	 * @param x Coordonn�e en abscisses pixel.
	 * @param y Coordonn�e en ordonn�es pixel.
	 * @return La norme u.
	 */
	public double getNormeU(double x, double y) {
		// Q-P
	    double dx1 = this.getX2() - this.getX1();
	    double dy1 = this.getY2() - this.getY1();
	    // X-P
	    double dx2 = x - this.getX1();
	    double dy2 = y - this.getY1();
	    // 
	    return (dx2 * dx1 + dy2 * dy1) / (dx1 * dx1 + dy1 * dy1);
	}
	
	/**
	 * Calcule la norme v pr�sente dans l'algorithme de Beier et Neely.
	 * @param x Coordonn�e en abscisses pixel.
	 * @param y Coordonn�e en ordonn�es pixel.
	 * @return La norme v.
	 */
	public double getNormeV(double x, double y) {
		// perpendiculaire(Q-P)
	    double dx1 = -(this.getY2() - this.getY1());
	    double dy1 = this.getX2() - this.getX1();
	    // X-P
	    double dx2 = x - this.getX1();
	    double dy2 = y - this.getY1();
	    
	    return (dx2 * dx1 + dy2 * dy1) / Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));
	}
	
	/**
     * Calcule le pixel de l'image de d�part correspondant au pixel de l'image d'arriv�e selon une ligne de contr�le.
     * @param pixelImageArrivee Pixel de l'image d'arriv�e � faire correspondre dans l'image de d�part.
     * @return Le pixel de l'image de d�part correspondant.
     */
	public PointControleSimple calcXiPrime(PointControleSimple pixelImageArrivee, PointControleDouble lignePrime) {
		double u = this.getNormeU(pixelImageArrivee.getX(), pixelImageArrivee.getY());
		double v = this.getNormeV(pixelImageArrivee.getX(), pixelImageArrivee.getY());
				
		PointControleSimple additionDeb = new PointControleSimple(lignePrime.getX1() + u * (lignePrime.getX2() - lignePrime.getX1()), lignePrime.getY1() + u * (lignePrime.getY2() - lignePrime.getY1()));
				
		double normeQpPp = lignePrime.getNorme();
				
		PointControleSimple perpQpPp = new PointControleSimple(lignePrime.getX2()-lignePrime.getX1(), lignePrime.getY2()-lignePrime.getY1());
		
		perpQpPp.tournerDe90DegresVersLaGauche();
		
		perpQpPp.setX(v*perpQpPp.getX());
        perpQpPp.setY(v*perpQpPp.getY());

        perpQpPp.setX(perpQpPp.getX()/normeQpPp);
        perpQpPp.setY(perpQpPp.getY()/normeQpPp);

        return new PointControleSimple(additionDeb.getX()+perpQpPp.getX(), additionDeb.getY()+perpQpPp.getY());
    }
}

