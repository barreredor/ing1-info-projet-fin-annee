package application.abstraction;

/**
 * Classe repr�sentant un point de contr�le simple.
 */
public final class PointControleSimple implements PointControle {
    private double x;
    private double y;
    
    /**
     * Constructeur de classe.
     * @param x Coordonn�e en abscisses du point.
     * @param y Coordonn�e en ordonn�es du point.
     */
    public PointControleSimple(double x, double y) {
        setX(x);
        setY(y);
    }
    
    /**
     * R�cup�re la coordonn�e en abscisses du point.
     * @return La coordonn�e en abscisses du point.
     */
    public double getX() {
        return x;
    }
    
    /**
     * R�cup�re la coordonn�e en ordonn�es du point.
     * @return La coordonn�e en ordonn�es du point.
     */
    public double getY() {
        return y;
    }
    
    /**
     * D�finit la coordonn�e en abscisses du point.
     * @param x La coordonn�e en abscisses du point.
     */
    public void setX(double x) {
        this.x = x;
    }
    
    /**
     * D�finit la coordonn�e en ordonn�es du point.
     * @param y La coordonn�e en ordonn�es du point.
     */
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public PointControle copie() {
        return new PointControleSimple(x, y);
    }
    
    /**
     * V�rifie si un point est �quivalent � ce point avec une certaine marge d'erreur.
     * @param point Le point � comparer.
     * @param seuil La marge d'erreur.
     * @return Vrai si les points sont �quivalents, sinon faux.
     */
    private Boolean estEquivalent(PointControleSimple point, double seuil) {
        double distanceAuCarree = Math.pow(distanceEuclidiene(this, point), 2);
        double seuilAuCarre = Math.pow(seuil, 2);
        return distanceAuCarree <= seuilAuCarre;
    }
    
    @Override
    public void harmoniser(PointControle point, double seuilEquivalence) {
        if (point instanceof PointControleSimple) {
            PointControleSimple pointAHarmoniser = (PointControleSimple) point;
            
            if (pointAHarmoniser.estEquivalent(this, seuilEquivalence)) {
                pointAHarmoniser.setX(this.x);
                pointAHarmoniser.setY(this.y);
            }
        }
    }
    
    @Override
    public String toString() {
        return "PointDeControleSimple{x=" + x + ", y=" + y + '}';
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PointControleSimple) {
            PointControleSimple point2 = (PointControleSimple) obj;
            return point2.getX() == getX() && point2.getY() == getY();
        } else {
            return false;
        }
    }
    
    /**
     * Calcule le k-i�me point entre deux points donn�s.
     * @param pointDepart Le point de d�part.
     * @param pointArrivee Le point d'arriv�e.
     * @param nombreEtapes Le nombre total d'�tapes.
     * @param k L'indice du point interm�diaire (entre 0 == pointDepart et nombreEtapes == pointArrivee).
     * @return Le k-i�me point entre les deux points donn�s.
     */
    public static PointControleSimple getKiemePointEntreDeuxPoints(PointControleSimple pointDepart, PointControleSimple pointArrivee, int nombreEtapes, int k) {
        double xIntermediaire = pointDepart.getX() + ((pointArrivee.getX() - pointDepart.getX()) * k) / nombreEtapes;
        double yIntermediaire = pointDepart.getY() + ((pointArrivee.getY() - pointDepart.getY()) * k) / nombreEtapes;
        return new PointControleSimple(xIntermediaire, yIntermediaire);
    }
    
    /**
     * Effectue une rotation de 90 degr�s vers la gauche du vecteur.
     */
    public void tournerDe90DegresVersLaGauche() {
        double tmp = this.getX();
        this.setX(-this.getY());
        this.setY(tmp);
    }
    
    /**
     * Calcule la distance euclidienne entre 2 points.
     * @param a PointControleSimple premier point de la comparaison.
     * @param b PointControleSimple deuxi�me point de la comparaison.
     * @return double la distance euclidienne entre ces deux points.
     */
    public static double distanceEuclidiene(PointControleSimple a, PointControleSimple b) {
        return Math.sqrt(Math.pow(a.getX()-b.getX(), 2) + Math.pow(a.getY()-b.getY(), 2));
    }
}