package morphing;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import application.abstraction.DonneesMorphing;
import application.abstraction.PointControle;
import exceptions.TentativeAccesElementInexistantException;
import gif.fmsware.AnimatedGifEncoder;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

/**
 * Classe abstraite permettant de g�n�raliser les m�thodes communes � tous les morphings.
 * @param <T> Le type de point de contr�le que le morphing utilisera (soit PointControleSimple soit PointControleDouble).
 */
public abstract class Morphing<T extends PointControle> {
    private ArrayList<RGB[][]> imagesDonneesRGB;
    protected ArrayList<ArrayList<T>> pointsControleDesImagesDonnees;
    private ArrayList<Image> imagesResultat;
    private int nombreImagesIntermediaires;
    
    /**
     * Constructeur de la classe Morphing.
     * @param donnee DonneesMorphing<T> Les donn�es de morphing avec des points de contr�le.
     * @param nombreImagesIntermediaires int Le nombre d'images interm�diaires pour le morphing.
     */
    public Morphing(DonneesMorphing<T> donnees, int nombreImagesIntermediaires) {
        imagesDonneesRGB = new ArrayList<>();
        imagesResultat = new ArrayList<>();
        this.nombreImagesIntermediaires = nombreImagesIntermediaires;
        
        // On r�cup�re tous les points de contr�le
        pointsControleDesImagesDonnees = new ArrayList<>();
        for(int i = 0; i < donnees.getNombreImages(); i++) {
            pointsControleDesImagesDonnees.add(new ArrayList<T>());
            for(int j = 0; j < donnees.getNombrePointsControle(); j++) {
                pointsControleDesImagesDonnees.get(i).add(donnees.getPointControleImage(i,j));
            }
        }
        
        // Ajout de la premi�re image d'origine
        imagesResultat.add(donnees.getImage(0));
        imagesDonneesRGB.add(transformerImageEnImageRGB(donnees.getImage(0)));
        for(int i = 1; i < donnees.getNombreImages(); i++) {
            // Cr�ation des images interm�diaires
            for(int j = 0; j < getNombreImagesIntermediaires(); j++) {
                imagesResultat.add(null);
            }
            // Ajout des autres images d'origine
            imagesResultat.add(donnees.getImage(i));
            imagesDonneesRGB.add(transformerImageEnImageRGB(donnees.getImage(i)));
        }
    }
    
    protected int getNombreImagesDonneesRGB() {
        return imagesDonneesRGB.size();
    }
    
    protected int getNombrePointsControle() {
        if (pointsControleDesImagesDonnees.size() == 0) {
            return 0;
        } else {
            return pointsControleDesImagesDonnees.get(0).size();
        }
    }
    
    public int getNombreImagesResultat() {
        return imagesResultat.size();
    }
    
    protected RGB[][] getImageDonneesRGB(int indiceImage) {
        if (indiceImage >= 0 && indiceImage < imagesDonneesRGB.size()) {
            return imagesDonneesRGB.get(indiceImage);
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }
    
    protected T getPointDeControleImage(int indiceImage, int indicePointControle) {
        if (indiceImage >= 0 && indiceImage < getNombreImagesDonneesRGB()) {
            if (indicePointControle >= 0 && indicePointControle < getNombrePointsControle()) {
                return pointsControleDesImagesDonnees.get(indiceImage).get(indicePointControle);
            } else {
                throw new TentativeAccesElementInexistantException("Point de contr�le inexistant");
            }
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }
    
    protected ArrayList<T> getPointsDeControleImage(int indiceImage) {
        if (indiceImage >= 0 && indiceImage < getNombreImagesDonneesRGB()) {
            return pointsControleDesImagesDonnees.get(indiceImage);
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }
    
    public Image getImageResultat(int indiceImage) {
        if (indiceImage >= 0 && indiceImage < imagesResultat.size()) {
            return imagesResultat.get(indiceImage);
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }
    
    protected Iterator<T> getIterateurListePointsControleImage(int indiceImage){
        return pointsControleDesImagesDonnees.get(indiceImage).iterator();
    }
    
    protected int getNombreImagesIntermediaires() {
        return nombreImagesIntermediaires;
    }
    
    protected void setImageResultat(int indiceImage, Image image) {
        if (indiceImage >= 0 && indiceImage < getNombreImagesResultat()) {
            imagesResultat.set(indiceImage, image);
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }
    
    /**
     * Calcule la k-i�me image interm�diaire entre l'image de d�part et l'image d'arriv�e.
     * @param k L'indice de l'image interm�diaire (1 <= k <= getNombreImagesIntermediaires()).
     * @param indiceImageDepart L'indice de l'image de d�part.
     * @param indiceImageArrivee L'indice de l'image d'arriv�e.
     * @return Les donn�es RGB de l'image interm�diaire g�n�r�e.
     */
    protected abstract RGB[][] genererKiemeImageEntreDeuxImages(int k, int indiceImageDepart, int indiceImageArrivee);
    
    /**
     * Calcule chaque image interm�diaire et les stocke dans imagesResultat.
     */
    public void genererImagesResultat() {
		for(int i = 1; i < this.getNombreImagesDonneesRGB(); i++) {
			for(int k = 1; k <= getNombreImagesIntermediaires(); k++) {
				this.setImageResultat((i-1)*getNombreImagesIntermediaires()+(i-1)+k, transformerImageRGBEnImage(genererKiemeImageEntreDeuxImages(k, i-1, i)));
			}
		}
	}
    
    /**
     * Permet de transformer une image de Image � RGB[][].
     * @param image L'image que l'on veut transformer.
     * @return La m�me image mais sous le type RGB[][].
     */
	public static RGB[][] transformerImageEnImageRGB(Image image){
        int largeur = (int) image.getWidth();
        int hauteur = (int) image.getHeight();
        
        RGB[][] img = new RGB[largeur][hauteur];
        PixelReader pixelReader = image.getPixelReader();
        for (int i = 0; i < largeur; i++) {
            for (int j = 0; j < hauteur; j++) {
            	int argb = pixelReader.getArgb(i, j);
                int alpha = (argb >> 24) & 0xFF;
                if (alpha == 0) { // On v�rifie si le pixel est enti�rement transparent pour le mettre blanc si c'est le cas
                    img[i][j] = new RGB(255, 255, 255);
                } else {
                    img[i][j] = new RGB(argb);
                }
            }
        }
        return img;
	}
	
	/**
	 * Permet de transformer une image de RGB[][] � Image.
	 * @param imageRGB Le tableau de RGB que l'on veut transformer.
	 * @return La m�me image mais sous le type Image.
	 */
	public static Image transformerImageRGBEnImage(RGB[][] imageRGB) {
	    WritableImage image = new WritableImage(imageRGB.length, imageRGB[0].length);
	    PixelWriter pixelWriter = image.getPixelWriter();
	    for(int i = 0; i < imageRGB.length; i++) {
	        for(int j = 0; j < imageRGB[0].length; j++) {
	            pixelWriter.setArgb(i, j, imageRGB[i][j].toARGB());
	        }
	    }       
	    return image;
	}
	
	/**
	 * Permet de transformer une image de Image � BufferedImage.
	 * @param image L'image que l'on veut transformer.
	 * @return La m�me image mais sous le type BufferedImage.
	 */
	private static BufferedImage transformerImageEnBufferedImage(Image image) {
	    int largeur = (int) image.getWidth();
	    int hauteur = (int) image.getHeight();
	    
	    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
	    PixelReader pixelReader = image.getPixelReader();
	    for (int i = 0; i < largeur; i++) {
	        for (int j = 0; j < hauteur; j++) {
	        	int argb = pixelReader.getArgb(i, j);
                int alpha = (argb >> 24) & 0xFF;
                if (alpha == 0) { // On v�rifie si le pixel est enti�rement transparent pour le mettre blanc si c'est le cas
                    bufferedImage.setRGB(i, j, new RGB(255, 255, 255).toARGB());
                } else {
                	bufferedImage.setRGB(i, j, argb);
                }
	        }
	    }
	    return bufferedImage;
	}

	/**
	 * Fonction permettant de cr�er le gif.
	 * @return Un tableau de BufferedImage.
	 */
	private BufferedImage[] creerTableauBufferedImagesDesImagesResultat() {
	    BufferedImage[] bufferedImage = new BufferedImage[this.getNombreImagesResultat()];
	    for(int i = 0; i < this.getNombreImagesResultat(); i++) {
	        bufferedImage[i] = transformerImageEnBufferedImage(this.getImageResultat(i));
	    }
	    return bufferedImage;
	}

	/**
	 * Sauvegarde les images calcul�es dans un gif qui sera enregistr� � l'emplacement chemin dans l'ordinateur.
	 * @param chemin Le chemin auquel on veut attribuer le gif ainsi calcul�.
	 * @param dureeGIF Le nombre de millisecondes entre chaque image du gif.
	 */
	public void sauvegarderGIF(String chemin, double dureeGIF) {
	    int delaiEntreImages = (int) Math.round((dureeGIF / getNombreImagesResultat()) * 1000);
	    BufferedImage[] tableau = this.creerTableauBufferedImagesDesImagesResultat();
	    AnimatedGifEncoder encodeur = new AnimatedGifEncoder();
	    
	    encodeur.start(chemin);
	    encodeur.setDelay(delaiEntreImages);
	    for(int i = 0; i < tableau.length; i++) {
	        encodeur.addFrame(tableau[i]);
	    }
	    encodeur.finish();
	}
}