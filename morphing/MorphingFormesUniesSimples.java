package morphing;

import application.abstraction.DonneesMorphingAvecPointsControleSimples;

/**
 * Classe pour le morphing de forme en polygone.
 */
public final class MorphingFormesUniesSimples extends MorphingFormesUnies {
    
    /**
     * Constructeur de la classe MorphingFormesUniesSimples.
     * @param donnee DonneesMorphingAvecPointsControleSimples Les donn�es de morphing avec des points de contr�le simples.
     * @param nombreImagesIntermediaires int Le nombre d'images interm�diaires pour le morphing.
     */
    public MorphingFormesUniesSimples(DonneesMorphingAvecPointsControleSimples donnee, int nombreImagesIntermediaires) {
        super(donnee, nombreImagesIntermediaires);
    }
}
