package morphing;

/**
 * Classe représentant une couleur.
 */
public class RGB {
	private int r;
	private int g;
	private int b;
	
	/**
     * Constructeur de la classe RGB prenant les composantes de couleur en paramètres.
     * @param r int La composante rouge de la couleur.
     * @param g int La composante verte de la couleur.
     * @param b int La composante bleue de la couleur.
     */
	public RGB(int r, int g, int b) {
		this.r = r % 256;
		this.g = g % 256;
		this.b = b % 256;
	}
	
	/**
     * Constructeur de la classe RGB prenant un entier représentant la couleur en paramètre.
     * @param color int L'entier représentant la couleur au format RGB.
     */
	public RGB(int color) {
        this((color & 0xff0000) >> 16, (color & 0xff00) >> 8, color & 0xff);
	}
	
	public int getR() {
		return r;
	}
	
	public int getG() {
		return g;
	}
	
	public int getB() {
		return b;
	}
	
	/**
     * Convertit la couleur en entier représentant le code RGB.
     * @return int L'entier représentant la couleur en format RGB 24 bits (en base 10).
     */
	public int toInt() {
		int rep = 0;
		rep += this.getB();
		rep += this.getG() << 8;
		rep += this.getR() << 16;
		return rep;
	}
	
	/**
     * Convertit la couleur en entier représentant le code ARGB.
     * @return int L'entier représentant la couleur en format ARGB 32 bits (en base 10).
     */
	public int toARGB() {
		return toInt() + (255 << 24);
	}
	
	@Override
	public String toString() {
		return "RGB[r="+getR()+", g="+getG()+", b="+getB()+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RGB) {
			RGB rgb2 = (RGB) obj;
			return rgb2.getR() == getR() && rgb2.getG() == getG() && rgb2.getB() == getB();
		} else {
			return false;
		}
	}
}
