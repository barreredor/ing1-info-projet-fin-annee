package morphing;

import java.util.HashSet;
import application.abstraction.DonneesMorphingAvecPointsControleDoubles;
import application.abstraction.PointControleDouble;
import application.abstraction.PointControleSimple;

public final class MorphingImages extends Morphing<PointControleDouble> {
	
	/**
	 * Constructeur de la classe MorphingImages
	 * @param donnee : contient l'ensemble des images et des points de contrle
	 */
	public MorphingImages(DonneesMorphingAvecPointsControleDoubles donnee, int nombreImagesIntermediaires) {
		super(donnee, nombreImagesIntermediaires);
	}

	@Override
	protected RGB[][] genererKiemeImageEntreDeuxImages(int indexImageIntermediaire, int indiceImageDepart, int indiceImageArrivee) {
	    int hauteur = (int) this.getImageResultat(0).getHeight();
	    int largeur = (int) this.getImageResultat(0).getWidth();
	    
	    // Ensemble des pixels d�j� utilis�s
	    HashSet<PointControleSimple> pixelsUtilisesDebut = new HashSet<>();
	    HashSet<PointControleSimple> pixelsUtilisesArrivee = new HashSet<>();

	    // Cr�er la nouvelle image
	    RGB[][] imageIntermediaire = new RGB[largeur][hauteur];

	    for (int y = 0; y < hauteur; y++) {
	        for (int x = 0; x < largeur; x++) {
	            double sommeDeplacementsXDebut = 0;
	            double sommeDeplacementsYDebut = 0;
	            double sommeDeplacementsXArrivee = 0;
	            double sommeDeplacementsYArrivee = 0;

	            double sommePoidsDebut = 0;
	            double sommePoidsArrivee = 0;

	            for (int i = 0; i < this.getNombrePointsControle(); i++) {
	                PointControleDouble ligneControleDepart = this.getPointDeControleImage(indiceImageDepart, i);
	                PointControleDouble ligneControleArrivee = this.getPointDeControleImage(indiceImageArrivee, i);

	                // Calcul de la ligne de contr�le interm�diaire
	                PointControleDouble ligneControleIntermediaire = kiemeLigneEntre(ligneControleDepart, ligneControleArrivee, this.getNombreImagesIntermediaires() + 2, indexImageIntermediaire);
	                
	                // D�formation de l'image de d�part
	                PointControleSimple pixelDeformeDebut = ligneControleIntermediaire.calcXiPrime(new PointControleSimple(x, y), ligneControleDepart);

	                double deplacementXDebut = pixelDeformeDebut.getX() - x;
	                double deplacementYDebut = pixelDeformeDebut.getY() - y;

	                double distanceDebut;
	                if (ligneControleDepart.getNormeU(x, y) < 0) {
	                    distanceDebut = Math.sqrt(Math.pow(ligneControleDepart.getX1() - x, 2) + Math.pow(ligneControleDepart.getY1() - y, 2));
	                } else if (ligneControleDepart.getNormeU(x, y) > 1) {
	                    distanceDebut = Math.sqrt(Math.pow(ligneControleDepart.getX2() - x, 2) + Math.pow(ligneControleDepart.getY2() - y, 2));
	                } else {
	                    distanceDebut = Math.abs(ligneControleIntermediaire.getNormeV(x, y));
	                }

	                // D�formation de l'image d'arriv�e
	                PointControleSimple pixelDeformeArrivee = ligneControleIntermediaire.calcXiPrime(new PointControleSimple(x, y), ligneControleArrivee);

	                double deplacementXArrivee = pixelDeformeArrivee.getX() - x;
	                double deplacementYArrivee = pixelDeformeArrivee.getY() - y;

	                double distanceArrivee;
	                if (ligneControleArrivee.getNormeU(x, y) < 0) {
	                    distanceArrivee = Math.sqrt(Math.pow(ligneControleArrivee.getX1() - x, 2) + Math.pow(ligneControleArrivee.getY1() - y, 2));
	                } else if (ligneControleArrivee.getNormeU(x, y) > 1) {
	                    distanceArrivee = Math.sqrt(Math.pow(ligneControleArrivee.getX2() - x, 2) + Math.pow(ligneControleArrivee.getY2() - y, 2));
	                } else {
	                    distanceArrivee = Math.abs(ligneControleIntermediaire.getNormeV(x, y));
	                }

	                // Gestion du poids des lignes de contr�le
	                double p = 0.1;
	                double a = 1;
	                double b = 1.9;
	                
	                double longueurLigneIntermediaire = ligneControleIntermediaire.getNorme();
	                double poidsDebut = Math.pow(Math.pow(longueurLigneIntermediaire, p) / (a + distanceDebut), b);
	                double poidsArrivee = Math.pow(Math.pow(longueurLigneIntermediaire, p) / (a + distanceArrivee), b);
	                
	                sommeDeplacementsXDebut += deplacementXDebut * poidsDebut;
	                sommeDeplacementsYDebut += deplacementYDebut * poidsDebut;
	                sommeDeplacementsXArrivee += deplacementXArrivee * poidsArrivee;
	                sommeDeplacementsYArrivee += deplacementYArrivee * poidsArrivee;
	                
	                sommePoidsDebut += poidsDebut;
	                sommePoidsArrivee += poidsArrivee;
	            }

	            double nouveauXDebut = x + sommeDeplacementsXDebut / sommePoidsDebut;
	            double nouveauYDebut = y + sommeDeplacementsYDebut / sommePoidsDebut;
	            double nouveauXArrivee = x + sommeDeplacementsXArrivee / sommePoidsArrivee;
	            double nouveauYArrivee = y + sommeDeplacementsYArrivee / sommePoidsArrivee;

	            int xArrondiDebut = (int) Math.round(nouveauXDebut);
	            int yArrondiDebut = (int) Math.round(nouveauYDebut);
	            int xArrondiArrivee = (int) Math.round(nouveauXArrivee);
	            int yArrondiArrivee = (int) Math.round(nouveauYArrivee);

	            // Correction des coordonn�es hors limites
	            xArrondiDebut = Math.max(0, Math.min(xArrondiDebut, largeur - 1));
	            yArrondiDebut = Math.max(0, Math.min(yArrondiDebut, hauteur - 1));
	            xArrondiArrivee = Math.max(0, Math.min(xArrondiArrivee, largeur - 1));
	            yArrondiArrivee = Math.max(0, Math.min(yArrondiArrivee, hauteur - 1));

	            PointControleSimple pixelDebut = new PointControleSimple(xArrondiDebut, yArrondiDebut);
	            PointControleSimple pixelArrivee = new PointControleSimple(xArrondiArrivee, yArrondiArrivee);

	            if (!pixelsUtilisesDebut.contains(pixelDebut) && !pixelsUtilisesArrivee.contains(pixelArrivee)) {
	                // R�cup�ration des couleurs des pixels des images de d�part et d'arriv�e
	                RGB couleurDebut = this.getImageDonneesRGB(indiceImageDepart)[xArrondiDebut][yArrondiDebut];
	                RGB couleurArrivee = this.getImageDonneesRGB(indiceImageArrivee)[xArrondiArrivee][yArrondiArrivee];

	                float coefficientDebut = ((float) this.getNombreImagesIntermediaires() - (float) indexImageIntermediaire) / (float) this.getNombreImagesIntermediaires();
	                float coefficientArrivee = (float) indexImageIntermediaire / (float) this.getNombreImagesIntermediaires();

	                pixelsUtilisesDebut.add(pixelDebut);
	                pixelsUtilisesArrivee.add(pixelArrivee);

	                // Fusion des deux images
	                RGB couleurFusionnee = new RGB(
	                    (int) (coefficientDebut * couleurDebut.getR() + coefficientArrivee * couleurArrivee.getR()),
	                    (int) (coefficientDebut * couleurDebut.getG() + coefficientArrivee * couleurArrivee.getG()),
	                    (int) (coefficientDebut * couleurDebut.getB() + coefficientArrivee * couleurArrivee.getB())
	                );

	                imageIntermediaire[x][y] = couleurFusionnee;
	            } else {
	                imageIntermediaire[x][y] = new RGB(255, 255, 255); // Couleur par d�faut pour les pixels non trait�s
	            }
	        }
	    }
	    
	    return imageIntermediaire;
	}
	
	/**
	 * Fonction retournant la ligne obtenue par extrapolation � la k-i�me position
	 * @param ligneImageDepart
	 * @param ligneImageArrivee
	 * @param nombreEtapes Le nombre total d'�tapes.
	 * @param k L'indice du point interm�diaire (entre 0 == pointDepart et nombreEtapes == pointArrivee).
	 */
	private PointControleDouble kiemeLigneEntre(PointControleDouble ligneImageDepart, PointControleDouble ligneImageArrivee, int nombreEtapes, int k) {
	    PointControleSimple extremaLigneDepart1 = ligneImageDepart.getPoint1();
	    PointControleSimple extremaLigneDepart2 = ligneImageDepart.getPoint2();
		    
	    PointControleSimple extremaLigneArrivee1 = ligneImageArrivee.getPoint1();
	    PointControleSimple extremaLigneArrivee2 = ligneImageArrivee.getPoint2();
	
	    PointControleSimple extremaLigneKieme1 = PointControleSimple.getKiemePointEntreDeuxPoints(extremaLigneDepart1, extremaLigneArrivee1, nombreEtapes, k);
	    PointControleSimple extremaLigneKieme2 = PointControleSimple.getKiemePointEntreDeuxPoints(extremaLigneDepart2, extremaLigneArrivee2, nombreEtapes, k);
		    
	    return new PointControleDouble(extremaLigneKieme1.getX(), extremaLigneKieme1.getY(), extremaLigneKieme2.getX(), extremaLigneKieme2.getY());
	}
}