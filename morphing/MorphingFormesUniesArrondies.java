package morphing;

import java.util.ArrayList;
import application.abstraction.DonneesMorphingAvecPointsControleSimples;
import application.abstraction.PointControleSimple;

/**
 * Classe pour le morphing de forme arrondie.
 */
public final class MorphingFormesUniesArrondies extends MorphingFormesUnies {
	
	/**
     * Constructeur de la classe MorphingFormesUniesArrondies.
     * @param donnee DonneesMorphingAvecPointsControleSimples Les donn�es de morphing avec des points de contr�le simples.
     * @param nombreImagesIntermediaires int Le nombre d'images interm�diaires pour le morphing.
     */
	public MorphingFormesUniesArrondies(DonneesMorphingAvecPointsControleSimples donnee, int nombreImagesIntermediaires) {
		super(donnee, nombreImagesIntermediaires);
		
		// On d�coupe les courbes de B�zier en segments
		for (int i = 0 ; i < getNombreImagesDonneesRGB() ; i++) {
			pointsControleDesImagesDonnees.set(i, decouperCourbesBezier(pointsControleDesImagesDonnees.get(i)));
		}
	}
	
	/**
	 * D�coupe les courbes de B�zier en segments.
	 * @param listePointsControleSimples ArrayList<PointControleSimple> La liste des courbes de B�zier repr�sentant la forme (le dernier point doit avoir les m�mes coordonn�es que le premier).
	 * @return ArrayList<PointControleSimple> La liste des segments repr�sentant la forme.
	 */
    protected ArrayList<PointControleSimple> decouperCourbesBezier(ArrayList<PointControleSimple> listePointsControleSimples) {
		ArrayList<PointControleSimple> polygone = new ArrayList<>();
        
        // Approximation des courbes de B�zier en plusieurs segments
        for (int i = 0; i < listePointsControleSimples.size(); i += 3) {
            if (i + 3 < listePointsControleSimples.size()) {
                PointControleSimple p0 = listePointsControleSimples.get(i);
                PointControleSimple p1 = listePointsControleSimples.get(i + 1);
                PointControleSimple p2 = listePointsControleSimples.get(i + 2);
                PointControleSimple p3 = listePointsControleSimples.get(i + 3);
                
                polygone.addAll(decouperCourbeBezier(new PointControleSimple(p0.getX(), p0.getY()), new PointControleSimple(p1.getX(), p1.getY()), new PointControleSimple(p2.getX(), p2.getY()), new PointControleSimple(p3.getX(), p3.getY())));
            }
        }
        
        return polygone;
    }
    
	/**
	 * D�coupe la courbe de B�zier en segments.
	 * @param p0 PointControleSimple Premier point de la courbe de B�zier.
	 * @param p1 PointControleSimple Deuxi�me point de la courbe de B�zier.
	 * @param p2 PointControleSimple Troisi�me point de la courbe de B�zier.
	 * @param p3 PointControleSimple Dernier point de la courbe de B�zier.
	 * @return Les points de contr�le repr�sentant la s�rie de segments rempla�ant la courbe de B�zier.
	 */
    private ArrayList<PointControleSimple> decouperCourbeBezier(PointControleSimple p0, PointControleSimple p1, PointControleSimple p2, PointControleSimple p3) {
        // Nombre de d�coupages � effectuer
        int nombreDecoupages = 20;
        ArrayList<PointControleSimple> pointsCourbeBezierDecoupee = new ArrayList<>();
        
        pointsCourbeBezierDecoupee.add(p0);
        
        // Boucle pour d�couper la courbe en segments
        for (int i = 1; i < nombreDecoupages; i++) {
            double t = ((double) i) / nombreDecoupages;
            double x = Math.pow(1 - t, 3) * p0.getX() + 3 * Math.pow(1 - t, 2) * t * p1.getX() + 3 * (1 - t) * Math.pow(t, 2) * p2.getX() + Math.pow(t, 3) * p3.getX();
            double y = Math.pow(1 - t, 3) * p0.getY() + 3 * Math.pow(1 - t, 2) * t * p1.getY() + 3 * (1 - t) * Math.pow(t, 2) * p2.getY() + Math.pow(t, 3) * p3.getY();
            pointsCourbeBezierDecoupee.add(new PointControleSimple(x, y));
        }
        
        pointsCourbeBezierDecoupee.add(p3);
        return pointsCourbeBezierDecoupee;
    }
}
